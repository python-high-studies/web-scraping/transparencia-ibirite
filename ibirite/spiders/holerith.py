from pathlib import Path
import re
import scrapy



class HolerithSpider(scrapy.Spider):
    name = "holerith"
    allowed_domains = ["pmibirite.geosiap.net.br"]

    custom_settings = {
		'DOWNLOAD_DELAY': 2,
        'RANDOMIZE_DOWNLOAD_DELAY': True,
    }
    # start_urls = ["https://pmibirite.geosiap.net.br/pmibirite/websis/portal_transparencia/financeiro/lei_acesso/lai_remuneracoes_ajax_grid.php"]

    def start_requests(self):
        url = "https://pmibirite.geosiap.net.br/pmibirite/websis/portal_transparencia/financeiro/lei_acesso/lai_remuneracoes_ajax_grid.php"
        method = "POST"

        # POST /pmibirite/websis/portal_transparencia/financeiro/lei_acesso/lai_remuneracoes_ajax_grid.php HTTP/1.1
        # Host: pmibirite.geosiap.net.br
        # User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0
        # Accept: application/json, text/javascript, */*; q=0.01
        # Accept-Language: en-GB,en;q=0.5
        # Accept-Encoding: gzip, deflate, br
        # Content-Type: application/x-www-form-urlencoded; charset=UTF-8
        # X-Requested-With: XMLHttpRequest
        # Content-Length: 186
        # Origin: https://pmibirite.geosiap.net.br
        # Connection: keep-alive
        # Referer: https://pmibirite.geosiap.net.br/pmibirite/websis/portal_transparencia/financeiro/contas_publicas/index.php?consulta=../lei_acesso/lai_remuneracoes
        # Cookie: pmibirite=ch5h7ffinmvqdp6unstpiekicd; highcontrast=no
        # Sec-Fetch-Dest: empty
        # Sec-Fetch-Mode: cors
        # Sec-Fetch-Site: same-origin

        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0",
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Accept-Language": "en-GB,en;q=0.5",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "X-Requested-With": "XMLHttpRequest",
            "Referer": "https://pmibirite.geosiap.net.br/pmibirite/websis/portal_transparencia/financeiro/contas_publicas/index.php?consulta=../lei_acesso/lai_remuneracoes",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin"
        }

        body = "exibeSalarioZerado=+inner+&cp_ano=2024&cp_mes=05&agrupa_cargo=0&detalhes_holerith=1&numero_conta=2000&exibe_chapa=1&tabela_organograma=GRH_Organograma&ds_schema=public&exibeSalarioBase=1"

        request = scrapy.Request(url, method=method, headers=headers, body=body, callback=self.parse)

        yield request

    def parse(self, response):

        # JSON
        selector = scrapy.Selector(text=response.text)

        # URL with details for each person
        details = selector.jmespath('data[*][-1]').xpath('//@onclick').getall()

        base = 'https://pmibirite.geosiap.net.br/pmibirite/websis/portal_transparencia/financeiro'

        # Groups the url after .. and before ')
        pattern = '\.\./(.*)\'\)'

        for detail in details:
            query = re.search(pattern, detail).group(1)
            url = f'{base}/{query}'

            request = scrapy.Request(
                url,
                callback=self.parse_holetith
            )

            yield request

    def parse_holetith(self, response):
        profile = self.parse_profile(response.css('.profile-user-info'))
        detail = self.parse_detail(response.css('#gridDetalhes'))

    def strip(self, values: list[str]):
        for value in values:
            yield value.strip()

    def currency(self, value):
        try:
            return float(value.replace(' ', '').replace(',', '.'))
        except ValueError:
            return float()

    def quantity(self, value):
        try:
            return int(value)
        except ValueError:
            return int()

    def parse_table(self, table):
        headers = table.xpath('//th/text()').getall()

        rows = table.xpath(
            '//tr[contains(@class, "success")'
            ' or contains(@class, "warning")'
            ' or contains(@class, "info")]'
        )

        casts = [
            str.strip,
            str.strip,
            self.quantity,
            self.currency,
            self.currency,
            self.currency
        ]

        table = [
            dict(self.parse_row(row, headers, casts))
            for row in rows
        ]

        return table, headers

    def parse_row(self, row, headers, casts):
        for cast, header, cell in zip(casts, headers, row.xpath('td')):
            value = cell.xpath('text()').get(default='')
            yield header, cast(value)

    def parse_profile(self, response):
        keys = self.strip(response.css('.profile-info-name::text').getall())
        values = self.strip(response.css('.profile-info-value::text').getall())
        profile = dict(zip(keys, values))

        return profile

    def parse_detail(self, response):
        table, headers = self.parse_table(response)

        total = response.css('.grh_online_soma').xpath(
            'td[contains(strong, "Total")]/following-sibling::td/text()'
        ).getall()

        base = response.css('.grh_online_soma').xpath(
            'td[contains(strong, "Base")]/following-sibling::td/text()'
        ).get()

        net = response.css('.grh_online_liquido td b::text').get()

        revenue, expenditure, _ = total

        detail = {
            'Detalhes': table,
            'Total': {
                headers[3]: self.currency(revenue),
                headers[4]: self.currency(expenditure)
            },
            'Salário Base': self.currency(base),
            'Líquido': self.currency(net),
        }

        return detail
