# Portal da Transparência da Prefeitura Municipal Ibirité - MG

Raspagem de dados de [remuneração](https://pmibirite.geosiap.net.br/pmibirite/websis/portal_transparencia/financeiro/contas_publicas/index.php?consulta=../lei_acesso/lai_remuneracoes) no portal da transparência da prefeitura municipal Ibirité em Minas Gerais.

# Licença
 Raspagem de dados do Portal da Transparência da Prefeitura Municipal Ibirité © 2024 por André Rocha está assinalado com a licença CC0 1.0 Universal. Para ver uma cópia dessa licença, acesse: https://creativecommons.org/publicdomain/zero/1.0/ ou abra o arquivo [LICENSE](LICENSE).